from flask import Flask
from bson.json_util import dumps
from people import People

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!!'


@app.route('/people/')
def people():
    address = People()
    addresses = address.getpeople()
    lister = dumps(addresses)
    return lister


if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)
